
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/game-pages/Daily.vue')
      },
      {
        path: 'card',
        component: () => import('pages/game-pages/Card.vue')
      },
      {
        path: 'lucky-spin',
        component: () => import('pages/game-pages/Spin.vue')
      },
    ]
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
