import axios from 'axios'

const getApi = (link) => {
    return new Promise((resolve, reject) => {
        axios.get(link)
            .then(res => {
                resolve(res.data)
            }).catch(err => {
                reject(err)
            })
    })
}

export default getApi